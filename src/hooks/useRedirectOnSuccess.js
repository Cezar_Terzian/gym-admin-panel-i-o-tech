import React from "react";
import { useNavigate } from "react-router-dom";

const useRedirectOnSuccess = (isSuccess, URL) => {
  const navigate = useNavigate();

  React.useEffect(() => {
    if (isSuccess) {
      navigate(URL);
    }
  }, [isSuccess, URL, navigate]);
};

export default useRedirectOnSuccess;
