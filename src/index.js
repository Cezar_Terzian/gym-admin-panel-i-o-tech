import React from "react";
import ReactDOM from "react-dom/client";
import { QueryClient, QueryClientProvider } from "react-query";

import Router from "./Router";
import "./index.scss";

import LoadingPage from "pages/LoadingPage/LoadingPage";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import axios from "axios";
import { baseURL } from "api/config";
axios.defaults.baseURL = baseURL;

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // retryDelay: 2000,
      // refetchOnMount: false,
      refetchOnWindowFocus: false,
      refetchOnReconnect: true,
      retry: (failureCount, error) => {
        if (failureCount > 0 && error?.response?.status === 404) {
          return false;
        }
        if (failureCount >= 2) {
          return false;
        }
        return true;
      },
    },
  },
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <React.Suspense fallback={<LoadingPage />}>
      <QueryClientProvider client={queryClient}>
        <Router />
        <ToastContainer />
      </QueryClientProvider>
    </React.Suspense>
  </React.StrictMode>
);
