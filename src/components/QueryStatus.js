import React from "react";
import MoonLoader from "react-spinners/MoonLoader";

const QueryStatus = ({ isLoading, isError }) => {
  if (isLoading) {
    return (
      <div
        style={{
          marginTop: "6rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <MoonLoader size={40} />
      </div>
    );
  }
  if (isError) {
    return (
      <p style={{ textAlign: "center", color: "red", marginTop: "6rem" }}>
        An Error occured, Please try again
      </p>
    );
  }
  return null;
};

export default QueryStatus;
