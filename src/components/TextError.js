import PropTypes from "prop-types";

const TextError = ({ children }) => {
  return <span style={{ color: "red", fontSize: "small" }}>{children}</span>;
};

TextError.propTypes = {
  children: PropTypes.node,
};

export default TextError;
