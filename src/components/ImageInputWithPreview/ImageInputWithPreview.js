import React from "react";

import classes from "./ImageInputWithPreview.module.scss";
import DEFAULT_IMG from "assets/images/default.jpg";
import EditIcon from "@mui/icons-material/Edit";

const ImageInputWithPreview = ({
  name,
  preview,
  onChange,
  className = "",
  ...props
}) => {
  return (
    <label
      htmlFor={name}
      className={`${classes.label} ${className}`}
      {...props}
    >
      <input
        id={name}
        type="file"
        name={name}
        accept="image/*"
        onChange={onChange}
      />

      <img className={classes.preview} src={preview ?? DEFAULT_IMG} alt="" />

      <div className={classes.layer}>
        <EditIcon className={classes.pen} />
      </div>
    </label>
  );
};

export default ImageInputWithPreview;
