import { ErrorMessage, Field, useField } from "formik";
import PropTypes from "prop-types";

import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";

import TextError from "./TextError";

const Input = (props) => {
  const { name, ...rest } = props;
  const [field] = useField(name);

  return (
    <Grid container direction="column">
      <Field {...field} {...rest} as={TextField} />
      <ErrorMessage
        name={name}
        render={(msg) => <TextError>{msg}</TextError>}
      />
    </Grid>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Input;
