import { useQuery } from "react-query";
import axios from "axios";
import {
  useAddMutation,
  useUpdateMutation,
  useDeleteMutation,
} from "./helpers";

const API = {
  GET_ALL: `/api/gym/clients`,
  GET_SINGLE_CLIENT: (id) => `/api/gym/clients/${id}`,
  ADD_CLIENT: `/api/gym/clients`,
  UPDATE_CLIENT: (id) => `/api/gym/clients/${id}`,
  DELETE_CLIENT: (id) => `/api/gym/clients/${id}`,
};

const KEY = "CLIENTS";

export const useGetAllClients = () =>
  useQuery(KEY, async () => {
    const { data } = await axios.get(API.GET_ALL);
    return data;
  });

export const useGetSingleClient = (client_id) =>
  useQuery(
    [KEY, client_id],
    async () => {
      const { data } = await axios.get(API.GET_SINGLE_CLIENT(client_id));
      return data;
    },
    {
      enabled: !!client_id,
    }
  );

export const useAddClient = () => useAddMutation(KEY, API.ADD_CLIENT);
export const useUpdateClient = (client_id) =>
  useUpdateMutation(KEY, API.UPDATE_CLIENT(client_id));
export const useDeleteClient = (client_id) =>
  useDeleteMutation(KEY, API.DELETE_CLIENT(client_id));
