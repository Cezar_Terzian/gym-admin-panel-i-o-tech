import { useQuery } from "react-query";
import axios from "axios";
import {
  useAddMutation,
  useUpdateMutation,
  useDeleteMutation,
} from "./helpers";

const API = {
  GET_ALL: `/api/gym/classes`,
  GET_SINGLE_CLASS: (id) => `/api/gym/classes/${id}`,
  ADD_CLASS: `/api/gym/classes`,
  UPDATE_CLASS: (id) => `/api/gym/classes/${id}`,
  DELETE_CLASS: (id) => `/api/gym/classes/${id}`,
};

const KEY = "CLASSES";

export const useGetAllClasses = () =>
  useQuery(KEY, async () => {
    const { data } = await axios.get(API.GET_ALL);
    return data;
  });

export const useGetSingleClass = (class_id) =>
  useQuery(
    [KEY, class_id],
    async () => {
      const { data } = await axios.get(API.GET_SINGLE_CLASS(class_id));
      return data;
    },
    {
      enabled: !!class_id,
    }
  );

export const useAddClass = () => useAddMutation(KEY, API.ADD_CLASS);
export const useUpdateClass = (class_id) =>
  useUpdateMutation(KEY, API.UPDATE_CLASS(class_id));
export const useDeleteClass = (class_id) =>
  useDeleteMutation(KEY, API.DELETE_CLASS(class_id));
