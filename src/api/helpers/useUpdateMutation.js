import { useQueryClient, useMutation } from "react-query";
import { toast } from "react-toastify";
import axios from "axios";

export const useUpdateMutation = (key, url, { onSuccess } = {}) => {
  const queryClient = useQueryClient();

  return useMutation(
    async (dataToSend) => {
      const { data } = await axios.put(url, dataToSend);
      return data;
    },
    {
      onSuccess: (data, variables) => {
        const { message } = data;
        toast.success(message || "Data has been updated successfully");
        if (key) {
          queryClient.invalidateQueries([key]);
        }
        onSuccess?.(data, variables);
      },
      onError: (err) => {
        const message =
          err?.response?.data?.message ||
          "An Error occured while updating the data";
        toast.error(message);
      },
    }
  );
};
