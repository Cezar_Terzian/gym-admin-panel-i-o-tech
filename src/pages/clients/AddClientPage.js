import React from "react";
import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import LoadingButton from "@mui/lab/LoadingButton";

import { useAddClient } from "api/clients";
import ClientForm from "./forms/ClientForm";
import { Formik, Form } from "formik";

import { getInitialValues, getValidationSchema } from "./forms/formUtils";
import useRedirectOnSuccess from "hooks/useRedirectOnSuccess";

const AddClientPage = () => {
  const { isLoading, mutate: addClient, isSuccess } = useAddClient();
  useRedirectOnSuccess(isSuccess, Routes.clients.url);

  const handleSubmit = (values) => {
    addClient(values);
  };

  return (
    <>
      <div className="page_header">
        <h3>Add Client</h3>
        <NavLink to={Routes.clients.url}>
          <Button variant="contained">Back</Button>
        </NavLink>
      </div>

      <Formik
        onSubmit={handleSubmit}
        initialValues={getInitialValues()}
        validationSchema={getValidationSchema()}
      >
        {(formik) => {
          return (
            <Form>
              <br />
              <ClientForm />
              <br />
              <div className="center_container">
                <LoadingButton
                  type="submit"
                  variant="contained"
                  loading={isLoading}
                >
                  Add
                </LoadingButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default AddClientPage;
