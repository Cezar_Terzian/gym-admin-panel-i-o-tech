import React from "react";
import { useGetSingleClient } from "api/clients";
import { useParams } from "react-router-dom";
import QueryStatus from "components/QueryStatus";

import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import EditClient from "./EditClient";
import DeleteClientButton from "./DeleteClientButton";

const ClientDetailsPage = () => {
  const { id } = useParams();
  const { data, isLoading, isError, isSuccess } = useGetSingleClient(id);

  if (!isSuccess) {
    return <QueryStatus isLoading={isLoading} isError={isError} />;
  }
  return (
    <>
      <div className="page_header">
        <h3>Client Details</h3>
        <div>
          <NavLink to={Routes.clients.url} style={{ marginRight: "1rem" }}>
            <Button variant="contained">Back</Button>
          </NavLink>
          <DeleteClientButton id={id} />
        </div>
      </div>

      <EditClient id={id} data={data} />
    </>
  );
};

export default ClientDetailsPage;
