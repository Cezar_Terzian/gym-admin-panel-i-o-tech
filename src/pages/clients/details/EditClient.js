import React from "react";

import LoadingButton from "@mui/lab/LoadingButton";

import { useUpdateClient } from "api/clients";
import ClientForm from "../forms/ClientForm";
import { Formik, Form } from "formik";

import { getInitialValues, getValidationSchema } from "../forms/formUtils";
import { Routes } from "utility/navigation/Routes";
import useRedirectOnSuccess from "hooks/useRedirectOnSuccess";

const EditClient = ({ id, data }) => {
  const { isSuccess, isLoading, mutate: updateClient } = useUpdateClient(id);
  useRedirectOnSuccess(isSuccess, Routes.clients.url);

  const handleSubmit = (values) => {
    updateClient(values);
  };

  return (
    <Formik
      onSubmit={handleSubmit}
      initialValues={getInitialValues(data)}
      validationSchema={getValidationSchema(true)}
    >
      {(formik) => {
        return (
          <Form>
            <br />
            <ClientForm />
            <br />
            <div className="center_container">
              <LoadingButton
                type="submit"
                variant="contained"
                loading={isLoading}
              >
                Save
              </LoadingButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default EditClient;
