import React from "react";

import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import LoadingButton from "@mui/lab/LoadingButton";

import { useDeleteClient } from "api/clients";

import useRedirectOnSuccess from "hooks/useRedirectOnSuccess";
import { Routes } from "utility/navigation/Routes";

const DeleteClientButton = ({ id }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const { isSuccess, isLoading, mutate: deleteClient } = useDeleteClient(id);
  useRedirectOnSuccess(isSuccess, Routes.clients.url);

  const handleClose = () => {
    if (!isLoading) {
      setIsOpen(false);
    }
  };

  const handleDelete = () => {
    deleteClient();
  };

  return (
    <>
      <Button variant="contained" color="error" onClick={() => setIsOpen(true)}>
        Delete
      </Button>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure you want to delete this client, this action won't be
            reverted.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} variant="contained">
            No
          </Button>
          <LoadingButton
            onClick={handleDelete}
            color="error"
            variant="contained"
            loading={isLoading}
          >
            Yes
          </LoadingButton>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default DeleteClientButton;
