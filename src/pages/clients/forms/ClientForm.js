import React from "react";
import Input from "components/Input";

import Grid from "@mui/material/Grid";

const ClientForm = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="full_name" label="Full Name" id="full_name" />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input
            name="mobile_number"
            label="Mobile Number"
            id="mobile_number"
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="address" label="Address" id="address" />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input
            name="subscription_plan"
            label="Subscription Plan"
            id="subscription_plan"
          />
        </Grid>
      </Grid>
    </>
  );
};

export default ClientForm;
