import * as Yup from "yup";

export const getInitialValues = (objectToEdit = null) => {
  if (!objectToEdit) {
    return {
      full_name: "",
      mobile_number: "",
      address: "",
      subscription_plan: "",
    };
  }

  return {
    full_name: objectToEdit.full_name ?? "",
    mobile_number: objectToEdit.mobile_number ?? "",
    address: objectToEdit.address ?? "",
    subscription_plan: objectToEdit.subscription_plan ?? "",
  };
};

export const getValidationSchema = (editMode = false) => {
  return Yup.object().shape({
    full_name: Yup.string().required("Name is Required"),
    mobile_number: Yup.string().required("Mobile number is Required"),
    address: Yup.string().required("Address is Required"),
    subscription_plan: Yup.string().required("Subscription Plan is Required"),
  });
};

// export const getDataToSend = (values) => {
//   const data = { ...values };
//   if (values.logo === "") {
//     delete data["logo"];
//   }
//   if (values.logo_hover === "") {
//     delete data["logo_hover"];
//   }

//   const formData = new FormData();
//   buildFormData(formData, data);
//   return formData;
// };
