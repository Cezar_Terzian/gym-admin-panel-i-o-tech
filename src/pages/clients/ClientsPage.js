import React from "react";
import { useGetAllClients } from "api/clients";

import DataTable from "react-data-table-component";

import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import QueryStatus from "components/QueryStatus";

import InfoIcon from "@mui/icons-material/Info";
import Avatar from "@mui/material/Avatar";

const getAvatarLetters = (name) => {
  if (typeof name !== "string") return "";
  return name
    .split(" ") // Split on space
    .slice(0, 2) // Take the first two substrings
    .map((str) => str.charAt(0).toUpperCase()) // convert each substring to the initial letters only
    .join(""); // join to get the final result
};

const columns = [
  {
    name: "Image",
    cell: (row) => <Avatar>{getAvatarLetters(row.full_name)}</Avatar>,
    width: "125px",
  },
  {
    name: "Name",
    selector: (row) => row.full_name,
  },
  {
    name: "Phone Number",
    selector: (row) => row.mobile_number,
  },
  {
    name: "Address",
    selector: (row) => row.address,
  },
  {
    name: "Subscription Type",
    selector: (row) => row.subscription_plan,
  },
  {
    cell: (row) => (
      <NavLink to={Routes.client_details.navTo(row.id)}>
        <InfoIcon />
      </NavLink>
    ),
    width: "70px",
  },
];

const ClientsPage = () => {
  const { data, isLoading, isError, isSuccess } = useGetAllClients();

  return (
    <>
      <div className="page_header">
        <h3>Clients</h3>
        <NavLink to={Routes.add_client.url}>
          <Button variant="contained">Add</Button>
        </NavLink>
      </div>
      {isSuccess ? (
        <DataTable columns={columns} data={data} noHeader responsive />
      ) : (
        <QueryStatus isLoading={isLoading} isError={isError} />
      )}
    </>
  );
};

export default ClientsPage;
