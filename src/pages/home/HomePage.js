import React from "react";

import LOGO from "assets/images/brand_logo.png";
import classes from "./HomePage.module.scss";

const HomePage = () => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        paddingTop: "5rem",
      }}
    >
      <img style={{ width: "min(100%, 250px)" }} src={LOGO} alt="LOGO" />
      <h3 style={{ textAlign: "center" }}>Welcome to the Admin Panel!</h3>
      <br />

      <h4>
        Author:{" "}
        <a
          href="https://www.linkedin.com/in/cezar-terzian/"
          target="_blank"
          rel="noreferrer noopener"
          className={classes.link}
        >
          Cezar Terzian
        </a>
      </h4>
    </div>
  );
};

export default HomePage;
