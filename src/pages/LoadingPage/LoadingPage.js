import React from "react";

import PulseLoader from "react-spinners/PulseLoader";

const LoadingPage = ({ style = {}, ...props }) => {
  return (
    <main
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        ...style,
      }}
    >
      <PulseLoader />
    </main>
  );
};

export default LoadingPage;
