import React from "react";
import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";

import Button from "@mui/material/Button";

import classes from "./NotFoundPage.module.scss";

const NotFoundPage = ({ buttonText = "Home", to = Routes.home.url }) => {
  return (
    <div className={classes.page}>
      <h1>404</h1>
      <h3>Page Not Found</h3>
      <NavLink to={to} className={classes.btn_container}>
        <Button variant="contained" color="primary">
          {buttonText}
        </Button>
      </NavLink>
    </div>
  );
};

export default NotFoundPage;
