import React from "react";
import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import LoadingButton from "@mui/lab/LoadingButton";

import { useAddClass } from "api/classes";
import ClassForm from "./forms/ClassForm";
import { Formik, Form } from "formik";

import {
  getDataToSend,
  getInitialValues,
  getValidationSchema,
} from "./forms/formUtils";
import useRedirectOnSuccess from "hooks/useRedirectOnSuccess";
import useImagePreview from "hooks/useImagePreview";

const AddClassPage = () => {
  const { isLoading, mutate: addClass, isSuccess } = useAddClass();
  useRedirectOnSuccess(isSuccess, Routes.classes.url);

  const { preview, handleImageChange } = useImagePreview(null);
  const handleSubmit = (values) => {
    addClass(getDataToSend(values));
  };

  return (
    <>
      <div className="page_header">
        <h3>Add Class</h3>
        <NavLink to={Routes.classes.url}>
          <Button variant="contained">Back</Button>
        </NavLink>
      </div>

      <Formik
        onSubmit={handleSubmit}
        initialValues={getInitialValues()}
        validationSchema={getValidationSchema()}
      >
        {(formik) => {
          return (
            <Form>
              <br />
              <ClassForm
                preview={preview}
                handleImageChange={handleImageChange}
              />
              <br />
              <div className="center_container">
                <LoadingButton
                  type="submit"
                  variant="contained"
                  loading={isLoading}
                >
                  Add
                </LoadingButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default AddClassPage;
