import React from "react";
import { useGetSingleClass } from "api/classes";
import { useParams } from "react-router-dom";
import QueryStatus from "components/QueryStatus";

import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import EditClass from "./EditClass";
import DeleteClassButton from "./DeleteClassButton";

const ClassDetailsPage = () => {
  const { id } = useParams();
  const { data, isLoading, isError, isSuccess } = useGetSingleClass(id);
  console.log(data);

  if (!isSuccess) {
    return <QueryStatus isLoading={isLoading} isError={isError} />;
  }
  return (
    <>
      <div className="page_header">
        <h3>Class Details</h3>
        <div>
          <NavLink to={Routes.classes.url} style={{ marginRight: "1rem" }}>
            <Button variant="contained">Back</Button>
          </NavLink>
          <DeleteClassButton id={id} />
        </div>
      </div>

      <EditClass id={id} data={data} />
    </>
  );
};

export default ClassDetailsPage;
