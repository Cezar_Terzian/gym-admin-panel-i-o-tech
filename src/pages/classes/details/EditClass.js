import React from "react";

import LoadingButton from "@mui/lab/LoadingButton";

import { useUpdateClass } from "api/classes";
import ClassForm from "../forms/ClassForm";
import { Formik, Form } from "formik";

import {
  getDataToSend,
  getInitialValues,
  getValidationSchema,
} from "../forms/formUtils";
import { Routes } from "utility/navigation/Routes";
import useRedirectOnSuccess from "hooks/useRedirectOnSuccess";
import useImagePreview from "hooks/useImagePreview";

const EditClass = ({ id, data }) => {
  const { isSuccess, isLoading, mutate: updateClass } = useUpdateClass(id);
  useRedirectOnSuccess(isSuccess, Routes.classes.url);

  const image = data?.image;
  const { preview, handleImageChange, setPreview } = useImagePreview(image);

  React.useEffect(() => {
    setPreview(image);
  }, [image, setPreview]);

  const handleSubmit = (values) => {
    updateClass(getDataToSend(values));
  };

  return (
    <Formik
      onSubmit={handleSubmit}
      initialValues={getInitialValues(data)}
      validationSchema={getValidationSchema(true)}
    >
      {(formik) => {
        return (
          <Form>
            <br />
            <ClassForm
              preview={preview}
              handleImageChange={handleImageChange}
            />
            <br />
            <div className="center_container">
              <LoadingButton
                type="submit"
                variant="contained"
                loading={isLoading}
              >
                Save
              </LoadingButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default EditClass;
