import React from "react";
import Input from "components/Input";

import Grid from "@mui/material/Grid";
import ImageInputWithPreview from "components/ImageInputWithPreview";
import { useFormikContext } from "formik";

const ClassForm = ({ preview, handleImageChange }) => {
  const formik = useFormikContext();

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
          <ImageInputWithPreview
            name={"image"}
            preview={preview}
            onChange={(e) => {
              handleImageChange(e);
              formik.setFieldValue("image", e.target.files[0]);
            }}
            style={{ height: "100%", width: "100%" }}
          />
        </Grid>

        <Grid item xs={12} sm={12} md={9} lg={9} xl={9}>
          <Input name="title" label="Title" id="title" />
          <br />
          <Input
            name="description"
            label="Description"
            id="description"
            multiline
            rows={8}
          />
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="coach_name" label="Coach Name" id="coach_name" />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="coach_brief" label="Coach Brief" id="coach_brief" />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="price" label="Price" id="price" type="number" />
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
          <Input name="timing" label="Timing" id="timing" />
        </Grid>
      </Grid>
    </>
  );
};

export default ClassForm;
