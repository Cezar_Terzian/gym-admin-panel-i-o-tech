import * as Yup from "yup";
// import { buildFormData } from "api/helpers";

export const getInitialValues = (objectToEdit = null) => {
  if (!objectToEdit) {
    return {
      title: "",
      coach_name: "",
      coach_brief: "",
      description: "",
      price: "",
      timing: "",

      image: "",
    };
  }

  return {
    title: objectToEdit.title ?? "",
    coach_name: objectToEdit.coach_name ?? "",
    coach_brief: objectToEdit.coach_brief ?? "",
    description: objectToEdit.description ?? "",
    price: objectToEdit.price ?? "",
    timing: objectToEdit.timing ?? "",

    image: "",
  };
};

export const getValidationSchema = (editMode = false) => {
  return Yup.object().shape({
    title: Yup.string().required("Title is Required"),
    coach_name: Yup.string().required("Coach Name is Required"),
  });
};

export const getDataToSend = (values) => {
  // const data = { ...values };
  // if (values.image === "") {
  //   delete data["image"];
  // }

  // const formData = new FormData();
  // buildFormData(formData, data);
  // return formData;

  const { image, ...data } = values;
  return data;
};
