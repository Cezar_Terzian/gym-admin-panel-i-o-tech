import React from "react";
import { useGetAllClasses } from "api/classes";

import DataTable from "react-data-table-component";

import { NavLink } from "react-router-dom";
import { Routes } from "utility/navigation/Routes";
import Button from "@mui/material/Button";
import QueryStatus from "components/QueryStatus";

import InfoIcon from "@mui/icons-material/Info";

const columns = [
  {
    name: "Title",
    selector: (row) => row.title,
  },
  {
    name: "Coach Name",
    selector: (row) => row.coach_name,
  },
  {
    name: "Price",
    selector: (row) => row.price,
  },
  {
    name: "Timing",
    selector: (row) => row.timing,
  },
  {
    cell: (row) => (
      <NavLink to={Routes.class_details.navTo(row.id)}>
        <InfoIcon />
      </NavLink>
    ),
    width: "70px",
  },
];

const ClassesPage = () => {
  const { data, isLoading, isError, isSuccess } = useGetAllClasses();

  return (
    <>
      <div className="page_header">
        <h3>Classes</h3>
        <NavLink to={Routes.add_class.url}>
          <Button variant="contained">Add</Button>
        </NavLink>
      </div>
      {isSuccess ? (
        <DataTable columns={columns} data={data} noHeader responsive />
      ) : (
        <QueryStatus isLoading={isLoading} isError={isError} />
      )}
    </>
  );
};

export default ClassesPage;
