import { useLocation } from "react-router-dom";
import { useEffect } from "react";

export const useBodyScrollResolver = () => {
  const { pathname } = useLocation();
  useEffect(() => {
    document.body.style.overflow = "auto";
  }, [pathname]);
};
