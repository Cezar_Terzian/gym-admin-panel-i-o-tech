export const isActive = (pathname, itemLink) => {
  if (itemLink === "/") return pathname === itemLink;
  return pathname.startsWith(itemLink);
};
