import { Routes } from "./Routes";
import HomeIcon from "@mui/icons-material/Home";
import GroupIcon from "@mui/icons-material/Group";
import SchoolIcon from "@mui/icons-material/School";

export const navigationConfig = [
  { title: "Home", navLink: Routes.home.url, Icon: <HomeIcon /> },
  { title: "Clients", navLink: Routes.clients.url, Icon: <GroupIcon /> },
  { title: "Classes", navLink: Routes.classes.url, Icon: <SchoolIcon /> },
];
