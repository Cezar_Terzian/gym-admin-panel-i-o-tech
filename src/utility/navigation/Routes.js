import { lazy } from "react";

export const Routes = {
  home: {
    url: "/",
    title: null,
    component: lazy(() => import("pages/home/HomePage")),
    appLayout: true,
  },
  clients: {
    url: "/clients",
    title: "Clients",
    component: lazy(() => import("pages/clients/ClientsPage")),
    appLayout: true,
  },
  add_client: {
    url: "/clients/add",
    title: "Add Client",
    component: lazy(() => import("pages/clients/AddClientPage")),
    appLayout: true,
  },
  client_details: {
    url: "/clients/:id",
    navTo: (id) => `/clients/${id}`,
    component: lazy(() => import("pages/clients/details/ClientDetailsPage")),
    appLayout: true,
  },
  add_class: {
    url: "/classes/add",
    title: "Add Class",
    component: lazy(() => import("pages/classes/AddClassPage")),
    appLayout: true,
  },
  class_details: {
    url: "/classes/:id",
    navTo: (id) => `/classes/${id}`,
    component: lazy(() => import("pages/classes/details/ClassDetailsPage")),
    appLayout: true,
  },

  classes: {
    url: "/classes",
    title: "Classes",
    component: lazy(() => import("pages/classes/ClassesPage")),
    appLayout: true,
  },
};
