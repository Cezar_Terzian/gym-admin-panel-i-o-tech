import React from "react";
import { lazy } from "react";
import { Routes, BrowserRouter as Router, Route } from "react-router-dom";
import { useScrollToTop } from "utility/navigation/useScrollToTop";
import { useBodyScrollResolver } from "utility/navigation/useBodyScrollResolver";

import { Routes as RoutesConfigs } from "utility/navigation/Routes";

import LoadingPage from "pages/LoadingPage/LoadingPage";

import Layout from "pages/Layout";

const NotFoundPage = lazy(() => import("./pages/404/NotFoundPage"));

const AppRouter = () => {
  return (
    <Router>
      <React.Suspense fallback={<LoadingPage />}>
        <AppRoutes />
      </React.Suspense>
    </Router>
  );
};
export default AppRouter;

function AppRoutes() {
  useScrollToTop();
  useBodyScrollResolver();

  return (
    <Routes>
      {Object.keys(RoutesConfigs).map((key) => {
        const { url, component: Component, appLayout } = RoutesConfigs[key];
        const Element = appLayout ? (
          <Layout>
            <React.Suspense
              fallback={
                <LoadingPage style={{ height: `calc(100vh - 64px)` }} />
              }
            >
              <Component />
            </React.Suspense>
          </Layout>
        ) : (
          <Component />
        );

        return <Route key={url} path={url} element={Element} />;
      })}
      <Route path="/*" element={<NotFoundPage />} />
    </Routes>
  );
}
